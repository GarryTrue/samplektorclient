package com.garrytrue.samplektorclient.ui

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.garrytrue.samplektorclient.R
import com.garrytrue.samplektorclient.models.Project
import kotlinx.android.synthetic.main.item_project.view.*


/**
 * Implementation of [ListAdapter] which works with our [Project] class
 *
 * @author Igor Torba
 */
internal class BrowseProjectsAdapter(
        diffCallback: DiffUtil.ItemCallback<Project> = DiffCallback) :
    ListAdapter<Project, ProjectViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProjectViewHolder =
        ProjectViewHolder(parent.inflateListItem(R.layout.item_project))

    override fun onBindViewHolder(holder: ProjectViewHolder, position: Int) =
        holder.bindTo(getItem(position))

}

/**
 * Implementation of [ViewHolder] which works with our [BrowseProjectsAdapter] class
 *
 * @author Igor Torba
 */
internal class ProjectViewHolder(
        itemView: View) : ViewHolder(itemView) {

    fun bindTo(project: Project) = with(itemView) {
        Glide.with(itemView.context).load(project.owner.ownerAvatar).into(itemView.projectAvatar)
        itemView.projectName.text = project.fullName
        itemView.ownerName.text = project.owner.ownerName
    }
}


// As Project is a data class so we can use equals for both methods
internal object DiffCallback : DiffUtil.ItemCallback<Project>() {
    override fun areItemsTheSame(oldItem: Project, newItem: Project): Boolean = oldItem == newItem

    override fun areContentsTheSame(oldItem: Project, newItem: Project): Boolean =
        oldItem == newItem
}

fun ViewGroup.inflateListItem(layoutRes: Int): View =
    LayoutInflater.from(context).inflate(layoutRes, this, false)