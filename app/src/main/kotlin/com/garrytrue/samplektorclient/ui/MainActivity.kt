package com.garrytrue.samplektorclient.ui

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import com.garrytrue.samplektorclient.ClientFactory
import com.garrytrue.samplektorclient.R
import com.garrytrue.samplektorclient.datasource.RemoteDataSource
import com.garrytrue.samplektorclient.interactors.GetProjectsInteractor
import com.garrytrue.samplektorclient.models.DataState
import com.garrytrue.samplektorclient.models.Project
import com.garrytrue.samplektorclient.viewmodels.BrowseProjectsViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.GlobalScope

const val TAG = "MainActivity"

class MainActivity : AppCompatActivity() {
    private lateinit var browseProjectsViewModel: BrowseProjectsViewModel
    private val adapter = BrowseProjectsAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()

        browseProjectsViewModel =
                BrowseProjectsViewModel(
                    GetProjectsInteractor(RemoteDataSource(ClientFactory()), GlobalScope))

        browseProjectsViewModel.liveData.observe(this, Observer { renderState(it) })

    }

    private fun setupRecyclerView() {
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }


    override fun onStart() {
        super.onStart()
        browseProjectsViewModel.fetchProjects()
    }


    private fun renderState(state: DataState<List<Project>>?) {
        when (state) {
            is DataState.Loading -> {
                progressBar.visibility = View.VISIBLE
                recyclerView.visibility = View.INVISIBLE
            }
            is DataState.Error -> {
                Log.d(TAG, state.error.message)
            }
            is DataState.Success -> {
                progressBar.visibility = View.INVISIBLE
                recyclerView.visibility = View.VISIBLE
                adapter.submitList(state.data)
            }
        }
    }
}
