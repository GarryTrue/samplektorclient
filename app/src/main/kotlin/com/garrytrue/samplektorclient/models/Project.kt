package com.garrytrue.samplektorclient.models

import com.google.gson.annotations.SerializedName

/**
 * Represents the Project, which returns remote source
 */
data class Project(
        val id: String, val name: String,
        @SerializedName("full_name") val fullName: String,
        @SerializedName("stargazers_count") val starCount: Int,
        @SerializedName("created_at") val dateCreated: String,
        val owner: OwnerModel)
