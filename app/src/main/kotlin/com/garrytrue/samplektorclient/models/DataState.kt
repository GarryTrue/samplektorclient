package com.garrytrue.samplektorclient.models

/**
 * Represents the current state of data fetch process
 * @param T type of data
 * @author Igor Torba
 */
sealed class DataState<out T> {
    data class Error<Throwable>(val error: kotlin.Throwable) : DataState<Throwable>()
    object Loading : DataState<Nothing>()
    data class Success<out T>(val data: T) : DataState<T>()
}