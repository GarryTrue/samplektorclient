package com.garrytrue.samplektorclient.models

data class ProjectsResponseModel(val items: List<Project>)
