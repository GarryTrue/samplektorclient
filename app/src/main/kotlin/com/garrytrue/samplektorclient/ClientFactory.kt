package com.garrytrue.samplektorclient

import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.features.json.GsonSerializer
import io.ktor.client.features.json.JsonFeature
import java.util.concurrent.TimeUnit

/**
 * Provides access to fully configured engines
 * @author Igor Torba
 */
const val CONNECT_TIMEOUT: Long = 20
const val READ_TIMEOUT: Long = 15

class ClientFactory {

    val cioClient: HttpClient by lazy {
        HttpClient(CIO) {
            engine {
                maxConnectionsCount = 16

                endpoint.apply {
                    connectTimeout = TimeUnit.SECONDS.toMillis(CONNECT_TIMEOUT).toInt()
                    connectRetryAttempts = 2
                }

            }
            install(JsonFeature) {
                serializer = GsonSerializer()
            }
        }
    }

    val okHttpClient: HttpClient by lazy {
        HttpClient(OkHttp) {
            engine {
                config {
                    connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    // can specify cache
                    // cache()
                }
                // https://square.github.io/okhttp/3.x/okhttp/okhttp3/Interceptor.html
                //                addInterceptor(interceptor)
                //                addNetworkInterceptor(interceptor)

            }
            install(JsonFeature) {
                serializer = GsonSerializer()
            }
        }
    }
}