package com.garrytrue.samplektorclient.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.garrytrue.samplektorclient.interactors.GetProjectsInteractor
import com.garrytrue.samplektorclient.models.DataState
import com.garrytrue.samplektorclient.models.Project

/**
 * Represent a fetch projects operation which will be used on a single screen.
 *
 * @author Igor Torba
 */
class BrowseProjectsViewModel constructor(
        private val getProjectsUseCase: GetProjectsInteractor) : ViewModel() {

    val liveData: MutableLiveData<DataState<List<Project>>> = MutableLiveData()

    override fun onCleared() {
        getProjectsUseCase.shutdown()
        super.onCleared()
    }


    fun fetchProjects() {
        liveData.postValue(DataState.Loading)
        getProjectsUseCase.execute(liveData)
    }
}