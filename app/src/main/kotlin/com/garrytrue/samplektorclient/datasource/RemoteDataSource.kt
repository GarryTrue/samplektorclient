package com.garrytrue.samplektorclient.datasource

import com.garrytrue.samplektorclient.ClientFactory
import com.garrytrue.samplektorclient.models.ProjectsResponseModel
import io.ktor.client.request.get
import io.ktor.client.request.url
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol

/**
 * Provides interface to works with remote data source
 * @author Igor Torba
 */
class RemoteDataSource(private val clientFactory: ClientFactory) : DataSource {

    private val urlBuilder = URLBuilder().apply {
        protocol = URLProtocol.HTTPS
        host = "api.github.com"
        path("search", "repositories")
        parameters.append("sort", "stars")
    }

    override suspend fun getProjectsByTech(techName: String): ProjectsResponseModel {
        urlBuilder.parameters.append("q", "language:$techName")
        return clientFactory.okHttpClient.get {
            url(urlBuilder.build().toString())
        }
    }
}