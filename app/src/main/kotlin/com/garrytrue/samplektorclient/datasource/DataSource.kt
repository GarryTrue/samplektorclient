package com.garrytrue.samplektorclient.datasource

import com.garrytrue.samplektorclient.models.ProjectsResponseModel

/**
 * Common interface for data sources
 * @author Igor Torba
 */
interface DataSource {
    suspend fun getProjectsByTech(techName: String): ProjectsResponseModel
}