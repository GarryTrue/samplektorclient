package com.garrytrue.samplektorclient.interactors

import android.arch.lifecycle.MutableLiveData
import com.garrytrue.samplektorclient.datasource.DataSource
import com.garrytrue.samplektorclient.models.DataState
import com.garrytrue.samplektorclient.models.Project
import kotlinx.coroutines.experimental.*
import kotlinx.coroutines.experimental.android.Main

/**
 * Knows where and how it should get projects
 * @author Igor Torba
 */

class GetProjectsInteractor(
        private val dataSource: DataSource,
        private val coroutineScope: CoroutineScope) {
    private lateinit var job: Job

    fun execute(liveData: MutableLiveData<DataState<List<Project>>>) {
        job = coroutineScope.launch(context = Dispatchers.Main) {
            val value = coroutineScope.async { dataSource.getProjectsByTech("kotlin") }.await()
            liveData.postValue(DataState.Success(value.items))
        }
    }


    fun shutdown() {
        job.cancelChildren()
    }

}